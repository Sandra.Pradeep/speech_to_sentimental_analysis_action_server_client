#! /usr/bin/env python

import rospy
# Brings in the SimpleActionClient
import actionlib
# Brings in the messages used by the speech action, including the
# goal message and the result message.
import speech2txt_action.msg


class SpeechAction(object):
    # create messages that are used to publish feedback/result
    _feedback = speech2txt_action.msg.SpeechFeedback()
    _result = speech2txt_action.msg.SpeechResult()
    #SimpleActionServer is created
    def __init__(self, name):
        self._action_name = name
        self._as = actionlib.SimpleActionServer(self._action_name, speech2txt_action.msg.SpeechAction, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()
    #execute callback function that we'll run everytime a new goal is received  
    def execute_cb(self, goal):
        # helper variables
        r = rospy.Rate(1)
        success = True
        
        
        # let the user know that the action is executing. 
        rospy.loginfo('performing sentimental analysis of text %s',goal.text)
        x=goal.text
       
        # check that preempt has not been requested by the client
        if self._as.is_preempt_requested():
                rospy.loginfo('%s: Preempted' % self._action_name)
                self._as.set_preempted()
                success = False
           
        #create list for sentimental analysis     
        nuetral=['hello','hai','day','time']
        positive=['good','smile','happy','beautiful']
        negative=['sad','kill','sorrow','cry']
        ## start executing the action
        for i in nuetral:
           if i==x:
               # publish the feedback
               self._feedback.sequence='It is a nuetral word'
               self._as.publish_feedback(self._feedback)
        for i in positive:           
           if i==x:
               self._feedback.sequence='It is a positive word'
               self._as.publish_feedback(self._feedback)
        for i in negative:           
           if i==x:
               self._feedback.sequence='It is a negative word'
               self._as.publish_feedback(self._feedback)
            # this step is not necessary, the sequence is computed at 1 Hz for demonstration purposes
        r.sleep()
          
        if success:
            #action server notifies the action client that the goal is complete
            self._result.sequence = self._feedback.sequence
            rospy.loginfo('%s: Succeeded' % self._action_name)
            self._as.set_succeeded(self._result)
#start of main function        
if __name__ == '__main__':
    rospy.init_node('speech')
    # creates the action server
    server = SpeechAction(rospy.get_name())
    rospy.spin()
