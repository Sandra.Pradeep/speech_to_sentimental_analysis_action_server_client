#! /usr/bin/env python
from __future__ import print_function
import rospy
# Brings in the SimpleActionClient
import actionlib
import speech_recognition as sr

# Brings in the messages used by the speech action, including the
# goal message and the result message.
import speech2txt_action.msg

def speech2txt_client():
    # Creates the SimpleActionClient, passing the type of the action
    # (SpeechAction) to the constructor.
    client = actionlib.SimpleActionClient('speech', speech2txt_action.msg.SpeechAction)
    
    # Waits until the action server has started up and started
    # listening for goals.
    client.wait_for_server()
    #speech recognition fuction
    r = sr.Recognizer()
    #make audio file as source
    with sr.WavFile("Recording_3.wav") as source:
         audio=r.record(source)
    #convert audio to text
    y=r.recognize_google(audio)
    # Creates a goal to send to the action server.
    goal = speech2txt_action.msg.SpeechGoal(text=y)
    #feedback=speech2txt_action.msg.SpeechFeedback(sequence=y)
    #client.send_feedback(feedback)
    # Sends the goal to the action server.
    client.send_goal(goal)
    

    # Waits for the server to finish performing the action.
    client.wait_for_result()

    # Prints out the result of executing the action
    return client.get_result() 
#start of main function
if __name__ == '__main__':
  while 1:
    try:
        # Initializes a rospy node so that the SimpleActionClient can
        # publish and subscribe over ROS.
        rospy.init_node('speech_client')
        result = speech2txt_client()
        print("Result:",result.sequence)
    except rospy.ROSInterruptException:
        print("program interrupted before completion", file=sys.stderr)

